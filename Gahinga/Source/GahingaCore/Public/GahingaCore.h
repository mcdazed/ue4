#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"

class FGahingaCore : public IModuleInterface
{
public:
	static inline FGahingaCore& Get()
	{
		return FModuleManager::LoadModuleChecked<FGahingaCore>("GahingaCore");
	}

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("GahingaCore");
	}

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
