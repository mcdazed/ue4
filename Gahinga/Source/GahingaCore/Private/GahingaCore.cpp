#include "GahingaCore.h"
#include "Modules/ModuleManager.h"

#include "Log.h"

void FGahingaCore::StartupModule()
{
	UE_LOG(LogGahingaCore, Log, TEXT("GahingaCore module starting up"));
}

void FGahingaCore::ShutdownModule()
{
	UE_LOG(LogGahingaCore, Log, TEXT("GahingaCore module shutting down"));
}

IMPLEMENT_PRIMARY_GAME_MODULE(FGahingaCore, GahingaCore, "GahingaCore");
