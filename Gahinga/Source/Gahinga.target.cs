using UnrealBuildTool;

public class GahingaTarget : TargetRules
{
	public GahingaTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "GahingaCore" } );
	}
}
