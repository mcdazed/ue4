using UnrealBuildTool;

public class GahingaEditorTarget : TargetRules
{
	public GahingaEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "GahingaCore" } );
	}
}
