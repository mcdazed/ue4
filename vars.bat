@echo off

set ROOTDIR=%~dp0
set ROOTDIR=%ROOTDIR:~0,-1%

set PROJECTID=
set UPROJ=%ROOTDIR%\%PROJECTID%.uproject
set BUILD=%ue4%\Build\BatchFiles\Build.bat
set REBUILD=%ue4%\Build\BatchFiles\Rebuild.bat
set EDITOR=%ue4%\Binaries\Win64\UE4Editor.exe
set UE4CMD=%ue4%\Binaries\Win64\UE4Editor-Cmd.exe