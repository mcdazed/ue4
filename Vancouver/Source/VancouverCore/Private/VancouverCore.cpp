#include "VancouverCore.h"
#include "Modules/ModuleManager.h"

#include "Log.h"

void FVancouverCore::StartupModule()
{
	UE_LOG(LogVancouverCore, Log, TEXT("VancouverCore module starting up"));
}

void FVancouverCore::ShutdownModule()
{
	UE_LOG(LogVancouverCore, Log, TEXT("VancouverCore module shutting down"));
}

IMPLEMENT_GAME_MODULE(FVancouverCore, VancouverCore);
