#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"

class FVancouverCore : public IModuleInterface
{
public:
	static inline FVancouverCore& Get()
	{
		return FModuleManager::LoadModuleChecked<FVancouverCore>("VancouverCore");
	}

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("VancouverCore");
	}

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
