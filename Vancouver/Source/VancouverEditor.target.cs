using UnrealBuildTool;

public class VancouverEditorTarget : TargetRules
{
	public VancouverEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "VancouverCore" } );
	}
}
