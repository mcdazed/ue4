using UnrealBuildTool;

public class VancouverTarget : TargetRules
{
	public VancouverTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "VancouverCore" } );
	}
}
