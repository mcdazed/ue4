using UnrealBuildTool;

public class BadGuys1EditorTarget : TargetRules
{
	public BadGuys1EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "BadGuys1Core" } );
	}
}
