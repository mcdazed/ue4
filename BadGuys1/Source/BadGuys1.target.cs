using UnrealBuildTool;

public class BadGuys1Target : TargetRules
{
	public BadGuys1Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "BadGuys1Core" } );
	}
}
