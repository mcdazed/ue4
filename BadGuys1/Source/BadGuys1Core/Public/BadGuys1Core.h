#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"

class FBadGuys1Core : public IModuleInterface
{
public:
	static inline FBadGuys1Core& Get()
	{
		return FModuleManager::LoadModuleChecked<FBadGuys1Core>("BadGuys1Core");
	}

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("BadGuys1Core");
	}

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
