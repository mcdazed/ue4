#include "BadGuys1Core.h"
#include "Modules/ModuleManager.h"

#include "Log.h"

void FBadGuys1Core::StartupModule()
{
	UE_LOG(LogBadGuys1Core, Log, TEXT("BadGuys1Core module starting up"));
}

void FBadGuys1Core::ShutdownModule()
{
	UE_LOG(LogBadGuys1Core, Log, TEXT("BadGuys1Core module shutting down"));
}

IMPLEMENT_PRIMARY_GAME_MODULE(FBadGuys1Core, BadGuys1Core, "BadGuys1Core");
